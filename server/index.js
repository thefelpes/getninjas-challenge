import Koa from 'koa';
import { getHeader, getFooter } from './utils/helpers';
import render from './utils/render';
import createElement from './utils/createElement';
import getFields from './utils/getFields';

const app = new Koa();

app.use(ctx => {
  const { _embedded } = getFields();
  const rendered = Object.keys(_embedded).map(

    // render the nodes of the _embedded obejct
    // each node is a fields array
    // -----------------------------------------------------------

    key => _embedded[key].map(field => render(createElement(field))).join('')
  ).join('');

  // return the http response with the rendered content
  // -----------------------------------------------------------

  ctx.body = `
    ${getHeader({ title: 'pedidos' })}
      <form>
        ${rendered}
        <button type="submit">Confirmar</button>
      </form>
    ${getFooter()}
  `;
});

app.listen(3000);
