export const getHeader = ({ title }) => `
  <!DOCTYPE html>
  <html>
    <head>
      <meta charset="UTF-8">
      <title>${title}</title>
    </head>
    <body>
`;

export const getFooter = () => `
    </body>
  </html>
`
