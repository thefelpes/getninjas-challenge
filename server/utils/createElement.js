/**
 * The types is a map of the html elements
 *
 * {
 *   "json-element-name": "html-tag-name"
 * }
 *
 */

const types = {
  enumerable: 'select',
  big_text: 'input',
  small_text: 'input',
  cep: 'input',
  email: 'input',
  phone: 'input',
};

/**
 * The props is a array of the html attributes
 * used on form fields
 *
 */

const props = ['name', 'required'];

/**
 * The children is a object that contains also methods
 * used to get element children.
 *
 * Example:
 * childrens.values() return a array of the <option> elements
 *
 */

const childrens = {

  /**
   * The values is a method used to get a array of the
   * option element's representation
   *
   * @param {Array} values The raw values that to be mapped to a element's representation
   * @return {Array} The array of the element's representation
   *
   */

  values({ values }) {
    const options = Object.keys(values).map(value => ({
      type: 'option',
      props: null,
      children: values[value],
    }));

    return [
      {
        type: 'option',
        props: null,
        children: '',
      },
      ...options
    ]
  },
}

/**
 * This function return a html tag name from the your name representation
 *
 * @param {String} type The name of your representation
 * @return {String} The html tag name
 *
 */

export const getElementType = type => {
  if (types[type]) {
    return types[type];
  }
  throw new Error(`element's type is invalid: ${type}`);
};

/**
 * This function return a object that contains the element's props
 *
 * @param {Object} description The representation of the element
 * @return {Object} The element's props (html attributes)
 *
 */

export const getElementProps = description => {
  const properties = {};

  if (description.label) {
    properties.id = description.name;
  }

  return Object.keys(description).reduce(
    (properties, prop) => {
      if (props.indexOf(prop) >= 0) {
        properties[prop] = description[prop];
      }
      return properties;
    },
    properties
  );
};

/**
 * This function return a element children
 *
 * @param {Object} description The representation of the element
 * @return {Array | Object | String} The element children
 *
 */

export const getElementChildren = description => (
  Object.keys(description).reduce((children, prop) => {
    if (childrens[prop]) {
      return childrens[prop](description);
    }
    return children;
  }, '')
);

/**
 * This function create a valid element to be rendered
 *
 * @param {Object} description The element representation
 * @return {Object} The element's object that can be rendered
 *
 */

const createElement = description => {
  const element = {
    type: getElementType(description.type),
    props: getElementProps(description),
    children: getElementChildren(description),
  };

  return description.label ? {
    type: 'div',
    props: {
      class: 'form-group',
    },
    children: [
      {
        type: 'label',
        props: {
          for: description.name,
        },
        children: description.label,
      },
      element
    ],
  } : element;
}

export default createElement;
