/**
 * Guarantees the valid the characters used on element.type
 *
 * @param {String} type The element's tag type
 * @returns {Boolean} The element validation
 */

// eslint-disable-next-line no-unused-vars
export const isElementTypeValid = type => true;

/**
 * Create the element's closed tag
 *
 * @param {Object} element The element's object representation
 * @returns {String} The element's closed tag
 */

export const closeTag = ({ type }) => {
  if (isElementTypeValid(type)) {
    return `</${type}>`;
  }
  throw new Error('invalid element type');
};

/**
 * Create the element's html properties
 *
 * @param {Object} element The element's object representation
 * @returns {String} The element properties as a string html
 */

export const createElementProps = ({ props }) => {
  if (props === null) {
    return '';
  }

  const keys = Object.keys(props);
  let propsString = ''

  for (const key of keys) {
    if (typeof props[key] === 'boolean') {
      propsString += ` ${key}`;
      continue;
    }

    propsString += ` ${key}="${props[key]}"`;
  }
  return propsString;
};

/**
 * Create a element's open tag and get your properties
 *
 * @param {Object} element The element's object representation
 * @return {String} The element tag as a string and your properties
 */

export const createTag = (element) => {
  if (isElementTypeValid(element.type)) {
    return `<${element.type}${createElementProps(element)}>`;
  }
  throw new Error('invalid element type');
};

/**
 * Create a html string from a data structure
 *
 * @param {Object} element The element's object representation
 * @return {String} The html's representation as a string
 */

const render = element => {
  if (typeof element === 'string') {
    return element;
  }

  let childrenList = '';
  if (Array.isArray(element.children)) {
    for (const current of element.children) {
      childrenList += render(current);
    }
  } else if (element.children !== undefined) {
    childrenList += render(element.children);
  }

  return `${createTag(element)}${childrenList}${closeTag(element)}`;
}

export default render;
