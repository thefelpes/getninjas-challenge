import createElement, { getElementType, getElementProps, getElementChildren } from './createElement';
import fixture from './fixture';

describe('createElement', () => {
  it('createElement should to be a function', () => {
    expect(typeof createElement).toBe('function');
  });

  it('snapshout of a element', () => {
    expect(createElement(fixture)).toMatchSnapshot();
  })

  it('getElementType should to return a element type', () => {
    expect(getElementType(fixture.type)).toBe('select');
  })

  it('getElementProps should to return a element props', () => {
    const expected = {
      id: 'Qual será o serviço?',
      name: 'Qual será o serviço?',
      required: true,
    };
    expect(getElementProps(fixture)).toEqual(expected);
  });

  it('getElementChildren should to return a element children', () => {
    const expected = [{
      type: 'option',
      props: null,
      children: '',
    }, {
      type: 'option',
      props: null,
      children: 'Coloração',
    }, {
      type: 'option',
      props: null,
      children: 'Corte',
    }, {
      type: 'option',
      props: null,
      children: 'Escova',
    }, {
      type: 'option',
      props: null,
      children: 'Escova progressiva/definitiva',
    }, {
      type: 'option',
      props: null,
      children: 'Luzes',
    }, {
      type: 'option',
      props: null,
      children: 'Manicure',
    }, {
      type: 'option',
      props: null,
      children: 'Pedicure',
    }, {
      type: 'option',
      props: null,
      children: 'Penteado',
    }];

    expect(getElementChildren(fixture)).toEqual(expected);
  })

  it('createElement should create a valid element representation', () => {
    const element = createElement(fixture);
    const expected = {
      type: 'div',
      props: {
        class: 'form-group',
      },
      children: [
        {
          type: 'label',
          props: {
            for: 'Qual será o serviço?'
          },
          children: 'Qual será o serviço?',
        },
        {
          type: 'select',
          props: {
            id: 'Qual será o serviço?',
            name: 'Qual será o serviço?',
            required: true,
          },
          children: [
            {
              type: 'option',
              props: null,
              children: '',
            }, {
              type: 'option',
              props: null,
              children: 'Coloração',
            }, {
              type: 'option',
              props: null,
              children: 'Corte',
            }, {
              type: 'option',
              props: null,
              children: 'Escova',
            }, {
              type: 'option',
              props: null,
              children: 'Escova progressiva/definitiva',
            }, {
              type: 'option',
              props: null,
              children: 'Luzes',
            }, {
              type: 'option',
              props: null,
              children: 'Manicure',
            }, {
              type: 'option',
              props: null,
              children: 'Pedicure',
            }, {
              type: 'option',
              props: null,
              children: 'Penteado',
            }
          ]
        }
      ]
    };
    expect(element).toEqual(expected);
  })
});
