import render, { closeTag, createElementProps, createTag } from './render';

describe('render', () => {
  it ('render should be a function', () => {
    expect(typeof render).toBe('function');
  });

  it('render should create a valid element', () => {
    const element = {
      type: 'select',
      props: {
        name: 'car-name',
        required: true,
      },
      children: [{
        type: 'option',
        props: null,
        children: {
          type: 'span',
          props: {
            class: 'span-class-test'
          },
          children: 'Jet Black',
        },
      }, {
        type: 'option',
        props: null,
        children: 'Ford Tough',
      }]
    };

    const expected = '<select name="car-name" required><option><span class="span-class-test">Jet Black</span></option><option>Ford Tough</option></select>';
    expect(render(element)).toBe(expected)
  })

  it('snapshot of a rendered element', () => {
    const element = {
      type: 'select',
      props: {
        name: 'car-name',
        required: true,
      },
      children: [{
        type: 'option',
        props: null,
        children: {
          type: 'span',
          props: {
            class: 'span-class-test'
          },
          children: 'Jet Black',
        },
      }, {
        type: 'option',
        props: null,
        children: 'Ford Tough',
      }]
    };

    expect(render(element)).toMatchSnapshot();
  });

  it('closeTag should create a valid closed tag', () => {
    const element = { type: 'div' };
    const closed = closeTag(element);
    expect(closed).toBe('</div>');
  })

  it('createElementProps should create valid html properties', () => {
    const element = {
      props: {
        class: 'class-test',
        name: 'name-test',
        id: 'id-test',
      },
    };

    const result = createElementProps(element);
    expect(result).toBe(' class="class-test" name="name-test" id="id-test"');
  });

  it('createTag should create a valid html tag', () => {
    const element = {
      type: 'select',
      props: {
        name: 'car-name',
        required: true,
      },
      children: [{
        type: 'option',
        props: null,
        children: {
          type: 'span',
          props: {
            class: 'span-class-test'
          },
          children: 'Jet Black',
        },
      }, {
        type: 'option',
        props: null,
        children: 'Ford Tough',
      }]
    };

    expect(createTag(element)).toBe('<select name="car-name" required>');
  });
})
