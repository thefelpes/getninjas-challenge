# SSR Vanilla.js

## Setup

### Downloading the code by cloning the repository:

```bash
git clone git@github.com:thefelpes/ssr-vanillajs.git
```

### Instalation

1. **Install Node.js (nvm is recommended)**: See [the nvm documentation](https://github.com/creationix/nvm) for instructions on installing it with your OS.

Once you have NVM installed locally, let's install the Node.js from `.nvmrc` file, and ensure that your npm is up-to-date.

```bash
npm install
```

```bash
npm install npm -g
```

Install the JavaScript dependencies:

```bash
npm install
```


### Running the tests

```bash
npm run test
```

```bash
npm run test:coverage
```

### Starting the server


Whenever you want to run in **production mode** you have to have **build the app** and to after **run the app**:

```bash
npm run build
```

```bash
npm start
```

Whenever you want to run in **development mode**:

```bash
npm run dev
```
